#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
 
int main()
{
    int sockfd,n,debug;
    int port = 8086;
    char sendline[100];
    char recvline[100];
    struct sockaddr_in servaddr;

    #ifdef DEBUG
    printf("DEBUG: main start\n");
    #endif
 
    sockfd=socket(AF_INET,SOCK_STREAM,0);

    #ifdef DEBUG
    printf("DEBUG: socket success: %d\n",sockfd);
    #endif

    bzero(&servaddr,sizeof servaddr);
 
    servaddr.sin_family=AF_INET;
    servaddr.sin_port=htons(port);
 
    debug=inet_pton(AF_INET,"127.0.0.1",&(servaddr.sin_addr));

    #ifdef DEBUG
    printf("DEBUG: %d %u\n",debug,servaddr.sin_addr.s_addr);
    #endif
 
    connect(sockfd,(struct sockaddr *)&servaddr,sizeof(servaddr));
    n=0;
    while(1)
    {
        bzero( sendline, 1000);
        bzero( recvline, 1000);
        #ifndef DEBUG
          fgets(sendline,1000,stdin); /*stdin = 0 , for standard input */
        #else
          //Actually, let's go Hello World for now
          char* hi = "Hello Swarmblock!\n";
          strncpy(sendline,hi,1000);
        #endif

	#ifdef DEBUG
        printf("DEBUG: %s",sendline);
	#endif
        write(sockfd,sendline,strlen(sendline));
        read(sockfd,recvline,1000);
        printf("%d: %s\n",n,recvline);
        n++;
    }
 
}
