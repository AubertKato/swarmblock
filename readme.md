# README

## Description

Code related to the Swarmblocks project

## Requirements

Java 1.8

## Compiling

Compiling is handled by Gradle (see <gradle.org>)  
The following three options are handled: eclipse, dist and buildJavadoc

### Generating an eclipse project

`./gradlew eclipse`  
Eclipse can then be used to compile the project.

### Generating an executable

`./gradlew dist`

### Generating javadoc

`./gradlew buildJavadoc`

## Running

Compiling with the dist option adds a script named `start.sh` in the build/dist folder.  
Eclipse can also be used to run the project.
