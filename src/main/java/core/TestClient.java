package core;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.StringReader;
import java.net.Socket;
import java.net.UnknownHostException;

public class TestClient extends Thread{
	
	Socket socket = null;
	
	public TestClient(String hostname,int portnumber){
		super("TestClient");
		try {
			socket = new Socket(hostname,portnumber);
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void run(){
		 try (
		            PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
		            BufferedReader in = new BufferedReader(
		                new InputStreamReader(socket.getInputStream()));
		        ) {
		            BufferedReader stdIn =
		                new BufferedReader(new StringReader("Dee dum\nDa dumm\nNEW###"));
		            String fromServer;
		            String fromUser;
		 
		            while ((fromUser = stdIn.readLine()) != null) {
		                System.out.println("Client: " + fromUser);
		                out.println(fromUser);
		                 
		                while((fromServer = in.readLine()) != null && !fromServer.equals(Server.DONE_SIGNAL) && !fromServer.equals(Server.CLOSE_SIGNAL)) {
		                	
		                    System.out.println("Server: " + fromServer);
		                	
		                }
		                System.out.println("end of message");
		            }
		            out.println(Server.CLOSE_SIGNAL);//TODO: might not work if server already closed?
		            socket.close();
		        } catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		 
	}
	
}
