package core;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;

public class ClientNicolas {

	public static void main(String[] argv) {

		Socket socket;
		int maxrepeat = 2;
		BufferedReader sin; // socket in
		PrintWriter sout; // socket out
        int port = 1703; // arbitrary

		try {
			socket = new Socket(InetAddress.getLoopbackAddress(), port);

			sin = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			sout = new PrintWriter(socket.getOutputStream());
			for (int repeat = 0; repeat < maxrepeat; repeat++) {
				for (int i = 0; i < 10; i++) {
					String out = recv_msg(sin);
					System.out.println("#" + repeat + ", received:" + i + ", length: " + out.length());
				}
				if (repeat < maxrepeat - 1) {
					System.out.println("[Waiting 10s]");
					Thread.sleep(1000 * 10);
                    System.out.println("[Send ACKnowledgement]");
					send_msg(sout, "ACK");
				}
			}
			socket.close();
		} catch (UnknownHostException e) {

			e.printStackTrace();
		} catch (IOException e) {

			e.printStackTrace();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private static String recv_msg(BufferedReader in) {
		int header_size = 8;

		String out_str = new String();
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		char[] header = new char[header_size];
		int msg_len;
		try {
			in.read(header, 0, header_size);
			String headerstr = new String(header);
			msg_len = Integer.parseInt(headerstr, 16);

			for (int j = 0; j < msg_len; j++) {
				out.write((byte) in.read());
			}
			out_str = out.toString("utf8");
		} catch (IOException e) {

			e.printStackTrace();
		}
		return out_str;
	}

	private static void send_msg(PrintWriter out, String msg) {
		int msg_len = msg.length();
		out.write(String.format("%08X", msg_len)); // Send prefix with message length in 8-character hexadecimal number
		out.write(msg);
		out.flush();
	}

}
