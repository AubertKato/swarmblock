package core;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;

import individuals.Individual;
import utils.Constants;

public class MicrolabServerThread extends Thread{
	
	public static final String NEW_INDIV = "NEW";
	public static final String MUTATE_INDIV = "MUTATE";
	public static final String EVALUATE_INDIV = "EVAL";
	public static final String SETUP_SIGNAL = "SETUP";
	
	protected static final int STANDBY = 0;
	protected static final int RECEIVINGINDIV = 1;
	protected static final int MUTATE = 2;
	protected static final int EVALUATE = 4;
	protected static final int CLOSING = -1;

	private Socket socket = null;
	private int state = STANDBY;
	
	private StringBuilder accumulator = new StringBuilder("");

	//Local storage of individuals to save bandwidth
	protected HashMap<String, Individual> individuals = new HashMap<String,Individual>();
	
		
	public MicrolabServerThread(Socket socket ){
		super("MicrolabServerThread");
		this.socket = socket;	
	}
	
	@Override
	public void run(){
		try (
	            PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
	            BufferedReader in = new BufferedReader(
	                new InputStreamReader(
	                    socket.getInputStream()));
	        ) {
			String inputLine, outputLine;
			while ((inputLine = in.readLine()) != null && (state != CLOSING)) {
				outputLine = dealWithData(inputLine);
				if(state != CLOSING)
					out.println(outputLine);
			}
			socket.close();
		} catch (IOException e){
			e.printStackTrace();
		}
	}
	
	protected String dealWithData(String readData){
		if(state == STANDBY) accumulator = new StringBuilder(""); // no need for memory
		
		if (Constants.debug) System.out.println("(Stub) Data was: "+readData);
		if(readData.contains(Server.separator)) {
			String[] parts = readData.split(Server.separator);
			switch(parts[0]) {
			case NEW_INDIV:
				if(standbycheck()) {
					makeNewIndiv();
				}
				break;
			case MUTATE_INDIV:
				if(standbycheck() && parts.length>1) {
					mutateIndiv(parts[1]);
				} else {
					state = MUTATE;
				}
				break;
			case EVALUATE_INDIV:
				if(standbycheck() && parts.length>1) {
					evaluateIndiv(parts[1]);
				} else {
					state = EVALUATE;
				}
				break;
			case SETUP_SIGNAL:
				System.out.println("STUB: received "+parts[0]+" signal");
				break;
			case Server.DONE_SIGNAL:
				if(state == MUTATE) {
					mutateIndiv(accumulator.toString());
				} else if(state == EVALUATE) {
					evaluateIndiv(accumulator.toString());
				}
				state = STANDBY;
				break;
			case Server.CLOSE_SIGNAL:
				state = CLOSING;
				break;
			default:
				System.err.println("Unknown signal "+parts[0]);
			}
		} else {
			accumulator.append(readData);
		}
		//accumulator.append("\n"+Server.DONE_SIGNAL);
	  return accumulator.toString()+"\n"+Server.DONE_SIGNAL;
	}
	
	protected boolean standbycheck() {
		if(state != STANDBY) {
			System.err.println("ERROR: SIGNAL RECEIVED WHILE NOT IN THE STANDBY STATE; INVALID SYNTAX");
		}
		
		return state == STANDBY;
	}
	
	protected void makeNewIndiv() {
		accumulator = new StringBuilder("");
		Individual i = Microlab.create();
		individuals.put(""+i.getID(), i);
		accumulator.append(i.toString());
	}
	
	protected void mutateIndiv(String ID) {
		accumulator = new StringBuilder("");
		Individual i = individuals.get(ID.trim());
		if(i != null) {
			Individual j = Microlab.mutate(i);
			individuals.put(""+j.getID(), j);
			accumulator.append(j.toString());
		}
		else {
			accumulator.append("ERROR"+Server.separator+"UNKNOWN ID");
		}
	}
	
	protected void evaluateIndiv(String ID) {
		accumulator = new StringBuilder("");
		Individual i = individuals.get(ID.trim());
		if(i != null) {
			Map<String,Double> evals = Microlab.evaluate(i);
			
			accumulator.append(evals.toString());
			
		}
		else {
			accumulator.append("ERROR"+Server.separator+"UNKNOWN ID");
		}
	}
}
