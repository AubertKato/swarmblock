package core;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import evaluator.descriptor.AbstractDescriptor;
import evaluator.descriptor.genotype.NodeNumber;
import individuals.Individual;
import generator.Generator;
import generator.Mutator;
import utils.Constants;
import utils.Tools;
import utils.Version;

public class Microlab {
	
	protected static Mutator defaultMutator = new Mutator();
	protected static Generator defaultGenerator = new Generator();
	

	
	public static String getVersion() { return Version.getVersion(); }

	public static void loadProperties(String filename){
		//Tools.stub(Microlab.class.toString(), "loadProperties");
		// mutate rate, #iterations, target (e.g. "target=center-line")
		BufferedReader br;
		try {
			br = new BufferedReader(new FileReader(filename));
		
			String line;
		    
			while((line = br.readLine()) != null){
				String[] paramPair = line.split("\\s*=\\s*");
				String trimmedParamName = paramPair[0].trim();
				if(trimmedParamName.startsWith("#") || paramPair.length != 2) continue; //Incorrect format or comment
				Tools.readConfigFromString(Constants.class,trimmedParamName, paramPair[1]);
			}
			br.close();
		} catch (FileNotFoundException e) {
			System.err.println("ERROR: loadProperties: file "+filename+" not found. Default parameters loaded instead.");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if(Constants.debug) System.out.println("DEBUG: Finished reading parameters");
	}
	public static Individual create(){
		Individual indiv = defaultGenerator.generate(Constants.initialNodesNumber, Constants.initialConnectionsNumber);
		if(Constants.debug) System.out.println("DEBUG: Individual generated");
		return indiv;
	}
	public static Individual mutate( Individual _ind ){
		Individual clone = defaultGenerator.jsonToIndividual(_ind.getReactionNetwork().toString());
		return defaultMutator.mutate(clone);
	}
	public static  Map<String,Double> evaluate( Individual _ind ){
		Tools.stub(Microlab.class.toString(), "evaluate");
		Map<String,Double> eval = new HashMap<String,Double>();
	    eval.put("fitness",(double)_ind.getID());
		return eval;
	}

	public static void main (String[] argv)
	{
	    loadProperties("test.param");
	    Individual indiv = create( );
	    
	    Individual bestIndiv = null;
	    Map<String,Double> bestEval = new HashMap<String,Double>();
	    bestEval.put("fitness",0.0);
	    
	    int iterations = 100;
	    
	    for ( int i = 0; i < iterations ; i++ )
	    {
	        Individual newIndiv = mutate( indiv);
	        Map<String,Double> newEval = evaluate( newIndiv);
	        if ( newEval!=null && (bestEval.get("fitness") <= newEval.get("fitness") ))
	        {
	            bestEval.put("fitness", newEval.get("fitness"));
	            bestIndiv = newIndiv;
	            indiv = newIndiv;
	        }
	    }
	    
	    System.out.println("Best individual: "+bestIndiv.getReactionNetwork().toString());

	}
}
