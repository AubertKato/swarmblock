package core;

import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;

import utils.Constants;

public class Server extends Thread{
	
	public static final String separator = "###";
	public static final String CLOSE_SIGNAL = "CLOSE";
	public static final String DONE_SIGNAL = "DONE"; //Transmission of a specific stream of data is complete

	private int portNumber = Constants.portNumber;
	
	private ServerSocket serverSocket = null;
	
	private boolean listening = true;
	
	public Server(){
		super("MainServerThread");
			try {
				serverSocket = new ServerSocket(portNumber,Constants.maxConnectionBacklog,InetAddress.getLoopbackAddress());
			} catch (IOException e) {
				e.printStackTrace();
			} 
		
	}
	
	/**
	 * Just stops accepting new requests and stop execution
	 */
	public void niceShutdown(){
		listening = false;
	}
	
	public void run(){
		while(listening){
			try {
				new MicrolabServerThread(serverSocket.accept()).start();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		try {
			serverSocket.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args){
		//First make a server
                System.out.println("This is getting ridiculous");
		Server serv = new Server();
                System.out.println("So far, so good");
		serv.start();
		System.out.println("Addr: "+serv.serverSocket.getInetAddress());
		//Then comes a client
		//TestClient test = new TestClient(serv.serverSocket.getInetAddress().getHostName(),serv.portNumber);
		
		//The client tries to connect!
		//test.start();
		
		//close the server
		//serv.niceShutdown();
	}
	
}
