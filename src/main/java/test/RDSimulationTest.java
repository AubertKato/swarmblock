package test;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;

import javax.swing.JFrame;

import evaluator.simulation.rd.RDConstants;
import evaluator.target.rd.RDFitnessFunctionIbuki;
import evaluator.target.rd.RDFitnessResult;
import evaluator.target.rd.RDPatternFitnessResultIbuki;
import individuals.reactionnetwork.ReactionNetwork;
import visualization.RDImagePanel;


public class RDSimulationTest {
	
	public static ReactionNetwork getNetwork(String filepath){
		ReactionNetwork reac=null;
		BufferedReader in;
		
		try {
			in = new BufferedReader(new FileReader(filepath));
			reac = utils.Tools.gson.fromJson(in, ReactionNetwork.class);
		} catch (FileNotFoundException e) {
			
			e.printStackTrace();
		}
		return reac;
	}
	
	public static void main(String[] args){
		utils.Constants.maxEvalTime = 100;
		String testSystem;
		if(args.length<1){
			System.err.println("RDSimulationTest: ERROR should specify network");
			System.exit(-1);
		}
		testSystem = args[0];
		ReactionNetwork reac = getNetwork(testSystem);
		
		boolean[][] target = RDPatternFitnessResultIbuki.getCenterLine();
		RDPatternFitnessResultIbuki.weightExponential = 0.1; //good candidate so far: 0.1 0.1
		RDConstants.matchPenalty=-0.1;
		RDFitnessFunctionIbuki fit = new RDFitnessFunctionIbuki(target);
		RDFitnessResult res = (RDFitnessResult) fit.evaluate(reac);
		
		
		RDImagePanel panel = new RDImagePanel(res.getConc());
		RDConstants.speciesOffset = 0;
		
		JFrame frame = new JFrame("test");
		frame.add(panel);
		frame.pack();
		frame.setVisible(true);
		
		
		
		
		System.out.println("Eval: "+res.getFitness());
	}

}
