/**
 * 
 */
package test.generator;

import static org.junit.Assert.*;

import java.util.HashSet;

import org.junit.BeforeClass;
import org.junit.Test;

import generator.Generator;
import individuals.Individual;
import individuals.reactionnetwork.Connection;
import individuals.reactionnetwork.Node;
import individuals.reactionnetwork.ReactionNetwork;

/**
 * @author naubertkato
 *
 */
public class GeneratorTest {
	
	static Generator generatorObject;
	static int maxSize = 10;
	static int maxSampling = 10;

	/**
	 * Creates the main generator object.
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		generatorObject = new Generator();
		utils.Constants.debug = true;
	}

	/**
	 * Check that the graph has the expected number of nodes, and the expected number of connections
	 */
	@Test(timeout=1000)
	public void testSize() {
		for(int node = 1; node < 10; node++) {
			for( int connection = node; connection <= node*node; connection++) {
				for(int attempt = 0; attempt < Math.min(connection,maxSampling); attempt++) { // multiple random tests, but not too many
					System.out.println("Generating indiv for node size "+node+" connection size "+connection);
					Individual ind = generatorObject.generate(node, connection);
					System.out.println("Generated indiv");
					assertNotEquals(ind,null);
					assertEquals(ind.getReactionNetwork().nodes.size(),node);
					assertEquals(ind.getReactionNetwork().connections.size(),connection);
				}
				
			}
			System.out.println("Done with node size "+node);
		}
	}
	
	/**
	 * Check that every node in a graph is reachable, or point to something.
	 * Inhibitors HAVE to be reachable.
	 */
	@Test
	public void testConnectivity() {
		for(int node = 1; node < 10; node++) {
			for( int connection = node; connection <= node*node; connection++) {
				for(int attempt = 0; attempt < connection; attempt++) { // multiple random tests, but not too many
					Individual ind = generatorObject.generate(node, connection);
					assertTrue(isConnected(ind.getReactionNetwork()));
				}
			}
		}
	}
	
	public boolean isConnected(ReactionNetwork rn) {
		HashSet<Node> reachable = new HashSet<Node>();
		HashSet<Node> reaching = new HashSet<Node>();
		for(Connection c: rn.connections) {
			reaching.add(c.from);
			reachable.add(c.to);
		}
		for(Node n: rn.nodes) {
			if(!reaching.contains(n)&&!reachable.contains(n)) {
				return false;
			}
		}
		return true;
	}

}
