package visualization;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;

import javax.swing.JPanel;

import evaluator.simulation.rd.RDConstants;


public class RDImagePanel extends JPanel {
	/**
	 * 
	 */
	private static final long serialVersionUID = -8782485912207043987L;
	
	protected float[][][] conc;
	
	public RDImagePanel(float[][][] res){
		this.conc=res;
	}
	
	public Dimension getPreferredSize(){
		return new Dimension(RDConstants.wsize, RDConstants.hsize);
	}
	
	public void paintComponent(Graphics g) {
		Color base = g.getColor();
		
		  for (int x = 0; x < conc[0].length; x++){
		    for (int y = 0; y < conc[0][x].length; y++){
		      float val = 0.0f;
		      float val2 = 0.0f;
		      float val3 = 0.0f;
		      
		      val = Math.min(1.0f,conc[0+RDConstants.speciesOffset][x][y]/RDConstants.concScale);
		      if (conc.length >= 2+RDConstants.speciesOffset) val2 = Math.min(1.0f,conc[1+RDConstants.speciesOffset][x][y]/RDConstants.concScale);
		      if (conc.length >= 3+RDConstants.speciesOffset) val3 = Math.min(1.0f,conc[2+RDConstants.speciesOffset][x][y]/RDConstants.concScale);
		      
		      g.setColor(new Color(Math.max(0.0f,val),Math.max(0.0f, val2),Math.max(0.0f,val3)));
		      g.fillRect((int)(x*RDConstants.spaceStep),(int)(y*RDConstants.spaceStep),(int)RDConstants.spaceStep,(int)RDConstants.spaceStep);
		    }
		  }
		  g.setColor(base);
	}
}
