package utils;

public class Constants {
	
	//Global
	public static boolean gui = false;
	public static boolean debug = false;
	
	//Initial indiv
	public static int initialNodesNumber = 1;
	public static int initialConnectionsNumber = 1;
	
	//Reaction Networks
	public static double minReactionNetworkTemplateValue = 0.0;
	public static double maxReactionNetworkTemplateValue = 200.0;
	public static double minReactionNetworkStabilityValue = 0.1; //very stable
	public static double maxReactionNetworkStabilityValue = 500.0; //very unstable
	public static int maxNetworkSize = 15;
	public static boolean use6_7rule = true; //stability calculation for inhibitors. Otherwise, stability is evolved
	
	//Evaluation
	public static int maxEvalTime = 1000;
	
	//Mutation
	public static int mutateParameterWeight = 87;
	public static int addAutocatalyticInputWeight = 1;
	public static int addTemplateWeight = 2;
	public static int addInhibitingTemplateWeight = 2;
	public static int removeTemplateWeight = 3;
	public static int splitTemplateWeight = 4;
	public static int continuousToThresholdedWeight = 1;
	public static double probGeneMutation = 0.1; //10%
	public static int portNumber = 8086;
	public static int maxConnectionBacklog = 50;
	
	
	

}
