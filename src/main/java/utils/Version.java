package utils;

public class Version {
	
	/**
	 * The build value is automatically updated by gradle upon compiling.
	 * The default value ("@VERSION@") means that compiling was done through other means.
	 */
	private static final String build = "@VERSION@";
	
	public static String getVersion(){
		return build;
	}

}
