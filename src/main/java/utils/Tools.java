package utils;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.Date;
import java.util.Random;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import individuals.reactionnetwork.Connection;
import individuals.reactionnetwork.ConnectionSerializer;
import individuals.reactionnetwork.ReactionNetwork;
import individuals.reactionnetwork.ReactionNetworkDeserializer;

public class Tools {
	
	public static Gson gson = new GsonBuilder().setPrettyPrinting()
			.registerTypeAdapter(ReactionNetwork.class, new ReactionNetworkDeserializer())
			.registerTypeAdapter(Connection.class, new ConnectionSerializer()).create();
	
	private static Random rand = new Random();
	
	public static double getRandomLinearScale(double lowerBound, double upperBound) {
		double lower = Math.min(lowerBound,upperBound);
		double upper = Math.max(lowerBound, upperBound); //just to be safe
		return lower + rand.nextDouble() * (upper - lower);
	}

	public static double getRandomLogScale(double lowerBound, double upperBound) {
		double lower = Math.log(lowerBound);
		double upper = Math.log(upperBound);
		return Math.exp(getRandomLinearScale(lower,upper));
	}
	
	public static double getGaussian(){
		return rand.nextGaussian();
	}
	
	
	public static String configsToString(){
		return configsToString(Constants.class);
	}
	
	public static String configsToString(Class<?> subConfigClass){
		StringBuilder sb = new StringBuilder();
		sb.append(new Date(System.currentTimeMillis()).toString()+"\n");
		
		Field[] f = subConfigClass.getFields();
		
		
		for(int i=0; i<f.length; i++){
			try {
				if(f[i].getType().isArray()){
					sb.append(f[i].getName()+"="+Arrays.deepToString((Object[]) f[i].get(null))+"\n");
				} else {
					sb.append(f[i].getName()+"="+f[i].get(null)+"\n"); //Only valid for static methods
				}
			} catch (IllegalArgumentException | IllegalAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		return sb.toString();
	}
	
	/**
	 * A parameter setting is formated as parameterName = parameterValue
	 * @param subConfigClass the class to update
	 * @param parameterName
	 * @param parameterValue
	 */
	public static void readConfigFromString(Class<?> subConfigClass, String parameterName, String parameterValue){
		Field[] fields = subConfigClass.getFields();
			
			
			for(int j = 0; j<fields.length; j++){
				if(fields[j].getName().trim().equals(parameterName.trim())){
					//We found the field, update the parameter
					//Now we need to parse correctly the parameter's value
					try {
						switch(fields[j].getType().toString()){
							case "Integer":
							case "int":
						
								fields[j].setInt(null, Integer.parseInt(parameterValue.trim()));
								break;
							case "Float":
							case "float":
						
								fields[j].setFloat(null, Float.parseFloat(parameterValue.trim()));
								break;
							case "Double":
							case "double":
						
								fields[j].setDouble(null, Double.parseDouble(parameterValue.trim()));
								break;
							case "Boolean":
							case "boolean":
						
								fields[j].setBoolean(null, Boolean.parseBoolean(parameterValue.trim()));
								break;
							case "String":
							case "class java.lang.String":
								fields[j].set(null, parameterValue.trim());
								break;
							case "String[]":
							case "class [Ljava.lang.String;":
								String base = parameterValue.trim(); // format: [ , ... , ]
								base = base.substring(1, base.length()-1); //removed surrounding brackets
								String[] vals = base.split("\\s*,\\s*");
								fields[j].set(null, vals);
								break;
							default:
								System.out.println(fields[j].getType().toString());
						
						}
					} catch (IllegalArgumentException | IllegalAccessException e) {
						
						e.printStackTrace();
					}
					
				}
			}
		
	}
	
	@SuppressWarnings("unchecked")
	public static <T> T loadNewInstance(String className){
		T object = null;
		try {
		    Class<?> cls = Class.forName(className.trim());
		    	
		    	object = (T) cls.newInstance();
		   
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException e) {
		    System.err.println("Class not found: "+className+" . Please check spelling");
		}
		
		return object;
	}
	
	@SuppressWarnings("unchecked")
	public static <T> T loadStaticFieldValue(String fieldName){
		T object = null;
		int index =  fieldName.lastIndexOf(".", 0);
		if(index < 0){
			System.err.println("Invalid field name: "+fieldName+" . Please check spelling");
		}
		String className = fieldName.substring(0,index);
		try {
			//Find the last dot, to get the class name
			
			String actualName = fieldName.substring(index);
		    Class<?> cls = Class.forName(className.trim());
		    	
		    	object = (T) cls.getField(actualName).get(null);
		   
		} catch (ClassNotFoundException  | IllegalAccessException | IllegalArgumentException | NoSuchFieldException | SecurityException e) {
		    System.err.println("Fitness function class not found: "+className+" . Please check spelling");
		}
		
		return object;
	}
	

	public static void main(String[] args) {
		for (int i = 0; i < 1000; i++) {
			System.out.println(getRandomLogScale(1, 60));
		}
	}
	
	public static void stub(String clazz, String function){
		System.out.println(clazz+": method "+function+" is a stub" );
	}

}
