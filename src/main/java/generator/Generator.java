package generator;

import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicInteger;

import individuals.Individual;
import individuals.reactionnetwork.Node;
import individuals.reactionnetwork.ReactionNetwork;
import utils.Constants;
import utils.Tools;

public class Generator {
	
	protected AtomicInteger nIndiv = new AtomicInteger(0);
	
	public Individual jsonToIndividual(String json){
		Individual indiv = new Individual(nIndiv.getAndIncrement());
		indiv.setReactionNetwork(Tools.gson.fromJson(json, ReactionNetwork.class));
		return indiv;
	}
	
	
	public Individual generate(int nodes, int connections){
		//Number of activations: has to be connections/nodes, or better
		//Obviously not possible if about nodes
		//TODO: note: actually, |connection| = |nodes| - 1 works...
		if(connections > nodes*nodes || nodes > connections){
			System.err.println(""+getClass().getName()+" generate: impossible node/connection requirements Nodes: "
		        +nodes+" Connections: "+connections);
			return null;
		}
		int minActivations = Math.max((connections+nodes-1)/nodes, nodes-connections/2);
		//we also need to check we have enough space in terms of activation templates to fit all inhibitions
		double minRequiredActivationCapacity = (Math.sqrt(1+4*nodes)-1.0)/2.0;
		minActivations = Math.max(minActivations, (int) Math.ceil(minRequiredActivationCapacity));
		int activations = (int) Tools.getRandomLinearScale(minActivations, nodes);//trick to get the upper bound of connections/nodes
		int inhibitions = nodes - activations;
		
		return detailedGenerate(activations, inhibitions, connections);
		
	}
	
	protected Individual detailedGenerate(int activations, int inhibitions, int connections){
		if(Constants.debug) System.out.println("    "+activations+" "+inhibitions);
		Individual indiv = new Individual(nIndiv.getAndIncrement());
		ArrayList<ArrayList<Integer>> who = new ArrayList<ArrayList<Integer>>();
		ArrayList<String> inhibitorNames = new ArrayList<String>();
		//First, make the activators
		for(int i = 0; i<activations; i++){
			String n = ""+i;
			indiv.addActivator(n);
			who.add(new ArrayList<Integer>());
		}
		
		//Second, decide how many additional connections each node will have.
		//Could use many different algorithm here, but let's keep it simple(ish)
		  // Approach: consider the activations * node rectangle, where we put a node if it exist and nothing otherwise
		  // Generate a random number of step forward, if full, keep stepping until the next empty space
		  // Note that we don't need to know who is going to be targeted by the inhibitors yet. That will be updated at the end.
		  // Also, inhibitors need at least one incoming connection, so we will start with that.
		
		for(int i = 0; i< inhibitions; i++){
			int inputNode = (int) Tools.getRandomLinearScale(0, activations);
			who.get(inputNode).add(activations+i);
		}
		
		// Next, we need at least inhibitions independent connections between activators
		
		for(int i = 0; i< inhibitions; i++){
			addConnectionToMatrix(who,activations);
		}
		
		
		
		//Next step: now that all connections are decided, build the inhibitors
		for(int i = 0; i<activations; i++){
			for(int j = 0; j<who.get(i).size(); j++){
				if(who.get(i).get(j)<activations){
					//this is an activation template
					indiv.addConnection(""+i, ""+who.get(i).get(j));
					indiv.addInhibitor(""+i, ""+who.get(i).get(j));
					inhibitorNames.add(Node.getInhibitorName(""+i, ""+who.get(i).get(j)));
				}
			}
		}
		
		
		// Finally all the other connections
		
		for(int i = 0; i< connections - 2 * inhibitions ; i++){ 
			addConnectionToMatrix(who,activations+inhibitions);
		}
		
		
		//Third step: build all connections
		for(int i = 0; i<activations; i++){
			for(int j = 0; j<who.get(i).size(); j++){
				if(who.get(i).get(j)<activations){ //this is an activator-activator connection
				  indiv.addConnection(""+i, ""+who.get(i).get(j)); //might be a repeat, so inefficient, but that it does not create problems
				} else {
					indiv.addConnection(""+i, inhibitorNames.get(who.get(i).get(j)-activations)); //inhibitors are indexed from 0 in inhibitorNames
				}
			}
		}
		
		return indiv;
	}
	
	
	/**
	 * Update the matrix representation of connections, with nodes valid outputs
	 * @param matrix
	 * @param nodes
	 */
	protected void addConnectionToMatrix(ArrayList<ArrayList<Integer>> matrix, int nodes){
		
		int activations = matrix.size();
		int combo =  (int) Tools.getRandomLinearScale(0, activations*nodes);
		int inputNode = combo/nodes;
		int outputNode = combo % nodes;
		while(matrix.get(inputNode).contains(outputNode)){
			combo++;
			combo %= activations*nodes;
			inputNode = combo/nodes;
			outputNode = combo % nodes;
		}
		matrix.get(inputNode).add(outputNode);
		
	}

	public static void main(String[] args){
	  Generator gen = new Generator();
	  for(int i = 1; i< 5; i++){
		  System.out.println("Individuals for node size "+i);
		  for(int j = i; j <= i*i; j++){
			  System.out.println("Connection size "+j);
			  Individual indiv = gen.generate(i, j);
			  System.out.println(indiv.getReactionNetwork().toString());
			  System.out.println("===================================================");
		  }
	  }
	  System.out.println("Done");
	  
	}
	
}
