package generator;

import individuals.Individual;

public interface MutationRule {
	
	public String getName();
	public int getWeight();
	public boolean isFeasible(Individual indiv);
	public Individual apply(Individual before); //modifications are DONE IN PLACE

}
