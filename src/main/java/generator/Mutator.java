package generator;

import java.util.ArrayList;

import generator.rules.AddAutocatalyticInput;
import generator.rules.AddInhibitingTemplate;
import generator.rules.AddTemplate;
import generator.rules.MutateParameter;
import generator.rules.SplitTemplate;
import individuals.Individual;
import utils.Tools;

public class Mutator {
	
	protected ArrayList<MutationRule> rules;
	
	public Mutator(ArrayList<MutationRule> rules){
		this.rules = rules;
	}
	
	public Mutator(){
		this.rules = new ArrayList<MutationRule>();
		rules.add(new MutateParameter());
		rules.add(new AddAutocatalyticInput());
		rules.add(new SplitTemplate());
		rules.add(new AddTemplate());
		rules.add(new AddInhibitingTemplate());
	}
	
	public Individual mutate(Individual mutant){
		//mutation is done in place
		int totalWeight = 0;
		for(int i = 0; i<rules.size(); i++){
			if(rules.get(i).isFeasible(mutant)) totalWeight += rules.get(i).getWeight();
		}
		int score = (int) Tools.getRandomLinearScale(0, totalWeight);
		int index = 0;
		while(score >= 0){
			if(rules.get(index).isFeasible(mutant)) score -= rules.get(index).getWeight();
			index++;
		}
		index--;
		rules.get(index).apply(mutant);
		return mutant;
	}

}
