package generator.rules;

import generator.MutationRule;

public abstract class BaseMutationRule  implements MutationRule{

	public String getName(){
		return getClass().getName(); //Trivial naming convention
	}
	
}
