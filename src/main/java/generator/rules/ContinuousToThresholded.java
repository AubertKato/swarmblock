package generator.rules;

import java.util.ArrayList;

import generator.MutationRule;
import individuals.Individual;
import individuals.reactionnetwork.Connection;
import individuals.reactionnetwork.Node;
import utils.Constants;


public class ContinuousToThresholded extends BaseMutationRule implements MutationRule {

	@Override
	public int getWeight() {
		
		return Constants.continuousToThresholdedWeight;
	}

	@Override
	public boolean isFeasible(Individual indiv) {
		int nTotal = indiv.getReactionNetwork().nodes.size();
		return (nTotal < Constants.maxNetworkSize-2); //we will add three new seqs
	}

	@Override
	public Individual apply(Individual before) {
		
		//Find all the good stuff.
		ArrayList<Connection> potentials = new ArrayList<Connection>();
		for(Connection c : before.getReactionNetwork().connections){
			//TODO:quid of the case where there is an inhibitor? Turn it into the bb inhib?
			if(c.to.type==Node.SIMPLE_SEQUENCE && c.inhib==null){ 
				potentials.add(c);
			}
		}
		
		if(potentials.size()==0){
			System.err.println("ContinuousToThreshold: actually no potential target");
			return before;
		}
		
		
		//take the connection
		
		//add a new autocat
		
		//and an inhibitor to that
		
		//if not already, make the destination auto cat
		
		//and also an inhibitor to that
		
		//connect all
		
		//remove the original connection
		
		return before;
	}

}
