package generator.rules;

import generator.MutationRule;
import individuals.Individual;
import individuals.reactionnetwork.Connection;
import individuals.reactionnetwork.Node;
import utils.Constants;
import utils.Tools;

public class SplitTemplate extends BaseMutationRule implements MutationRule {

	@Override
	public int getWeight() {
		
		return Constants.splitTemplateWeight;
	}

	@Override
	public boolean isFeasible(Individual indiv) {
		
		return indiv.getReactionNetwork().nodes.size()<Constants.maxNetworkSize;
	}

	@Override
	public Individual apply(Individual before) {
		//First, pick a connection
		int nConnections = before.getReactionNetwork().connections.size();
		Connection c = before.getReactionNetwork().connections.get((int)Tools.getRandomLinearScale(0, nConnections));
		String name = before.addActivator();
		before.addConnection(name, c.to.name);
		before.addConnection(c.from.name, name);
		if(c.inhib!=null){
		  Node inhib = c.inhib;
		  before.addInhibitor(c.from.name, name);
		  Node newInhib = before.getReactionNetwork().getNodeByName(Node.getInhibitorName(c.from.name, name));
		  if(newInhib != null){
			  for(Connection connex : before.getReactionNetwork().connections){
				  if(connex.to.equals(inhib)) connex.to = newInhib; //update all references
			  }
			  before.getReactionNetwork().nodes.remove(inhib);
		  } else {
			  System.err.println("Incorrect inhibition copy in split template");
		  }
		  
		}
		before.removeConnection(c.from.name, c.to.name);
		return before;
	}

}
