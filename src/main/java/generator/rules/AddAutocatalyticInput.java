package generator.rules;

import generator.MutationRule;
import individuals.Individual;
import utils.Constants;
import utils.Tools;

public class AddAutocatalyticInput extends BaseMutationRule implements MutationRule {

	@Override
	public int getWeight() {
		
		return Constants.addAutocatalyticInputWeight;
	}

	@Override
	public boolean isFeasible(Individual indiv) {
		
		return indiv.getReactionNetwork().nodes.size()<Constants.maxNetworkSize;
	}

	@Override
	public Individual apply(Individual before) {
		String name = before.addActivator();
		before.addConnection(name, name);//add an autocatalyst.
		//Then pick a random node, and connect to that
		int indexTo = (int) Tools.getRandomLinearScale(0, before.getReactionNetwork().nodes.size()-1); //we don't want to pick ourselves randomly
		String nodeTo = before.getReactionNetwork().nodes.get(indexTo).name;
		if(name.equals(nodeTo)) nodeTo =  before.getReactionNetwork().nodes.get(indexTo+1).name; //in case name isn't the last in the list
		before.addConnection(name, nodeTo);
		return before;
	}

}
