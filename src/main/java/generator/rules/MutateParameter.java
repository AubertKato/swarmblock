package generator.rules;

import generator.MutationRule;
import individuals.Individual;
import individuals.reactionnetwork.Connection;
import individuals.reactionnetwork.Node;
import utils.Constants;
import utils.Tools;

public class MutateParameter extends BaseMutationRule implements MutationRule {


	@Override
	public int getWeight() {
		
		return Constants.mutateParameterWeight;
	}

	@Override
	public boolean isFeasible(Individual indiv) {
		// always possible
		return true;
	}

	@Override
	public Individual apply(Individual indiv) {
		//We should already be dealing with a clone
		//Possible parameters: activator stabilities and template concentrations
		for (Node node : indiv.getReactionNetwork().nodes) {
			if (node.type == Node.SIMPLE_SEQUENCE || !utils.Constants.use6_7rule) {
				if (Tools.getRandomLinearScale(0, 1) < Constants.probGeneMutation) {
					node.parameter = mutateParam(node.parameter, Constants.minReactionNetworkStabilityValue, Constants.maxReactionNetworkStabilityValue);
				}
				if (node.hasPseudoTemplate && (Tools.getRandomLinearScale(0, 1) < Constants.probGeneMutation)) {
					node.pseudoTemplateConcentration = mutateParam(node.parameter, Constants.minReactionNetworkTemplateValue, Constants.maxReactionNetworkTemplateValue);
				}
			}
		}
		for (Connection conn : indiv.getReactionNetwork().connections) {
			if (conn.enabled && (Tools.getRandomLinearScale(0, 1) < Constants.probGeneMutation)) {
				conn.parameter = mutateParam(conn.parameter, Constants.minReactionNetworkTemplateValue, Constants.maxReactionNetworkTemplateValue);
			}
		}
		return indiv;
	}
	
	private double mutateParam(double oldParam, double min, double max) {
		double randGaussian = Tools.getGaussian();
		double mutatedParam = Math.exp(Math.log(oldParam) * (1 + randGaussian * 0.2)) + randGaussian * 2;
		if (mutatedParam < min)
			return min;
		if (mutatedParam > max)
			return max;
		return mutatedParam;
	}
	
	public static void main(String[] args){
		MutateParameter mp = new MutateParameter();
		System.out.println(mp.getName());
	}

}
