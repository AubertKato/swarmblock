package generator.rules;

import java.util.ArrayList;

import generator.MutationRule;
import individuals.Individual;
import individuals.reactionnetwork.Connection;
import individuals.reactionnetwork.Node;
import utils.Constants;
import utils.Tools;


public class AddInhibitingTemplate extends BaseMutationRule implements MutationRule {

	@Override
	public int getWeight() {
		
		return Constants.addInhibitingTemplateWeight;
	}

	@Override
	public boolean isFeasible(Individual indiv) {
		boolean inhibitableConnection = false;
		for(Connection c : indiv.getReactionNetwork().connections){
			if(c.to.type==Node.SIMPLE_SEQUENCE && c.inhib==null){
				inhibitableConnection = true;
				break;
			}
		}
		int nTotal = indiv.getReactionNetwork().nodes.size();
		return inhibitableConnection && (nTotal < Constants.maxNetworkSize); //the graph is not maxed out
	}

	@Override
	public Individual apply(Individual before) {
		
		//Find all the good stuff.
		ArrayList<Connection> potentials = new ArrayList<Connection>();
		for(Connection c : before.getReactionNetwork().connections){
			if(c.to.type==Node.SIMPLE_SEQUENCE && c.inhib==null){
				potentials.add(c);
			}
		}
		
		if(potentials.size()==0){
			System.err.println("AddInhibitingTemplate: actually no potential target");
			return before;
		}
		
		int indexConnection =  (int) Tools.getRandomLinearScale(0, potentials.size());
		Connection target = potentials.get(indexConnection);
		
		int nActivations = before.getReactionNetwork().getNSimpleSequences();
		
		int originIndex = (int) Tools.getRandomLinearScale(0, nActivations);
		int counter = -1;
		Node origin = null;
		int fr = -1;
		do{
			fr++;
			if(before.getReactionNetwork().nodes.get(fr).type!=Node.SIMPLE_SEQUENCE)
				continue; //not an activator, so skip
			
			counter++;
		}while(counter<originIndex);
		origin = before.getReactionNetwork().nodes.get(fr);
		
		before.addInhibitor(target.from.name, target.to.name);
		before.addConnection(origin.name, target.inhib.name);//now target has an inhibitor
		
		return before;
	}

}
