package generator.rules;

import generator.MutationRule;
import individuals.Individual;
import individuals.reactionnetwork.Node;
import utils.Constants;
import utils.Tools;


public class AddTemplate extends BaseMutationRule implements MutationRule {

	@Override
	public int getWeight() {
		
		return Constants.addTemplateWeight;
	}

	@Override
	public boolean isFeasible(Individual indiv) {
		int nConnections = indiv.getReactionNetwork().connections.size();
		int nActivations = indiv.getReactionNetwork().getNSimpleSequences();
		int nTotal = indiv.getReactionNetwork().nodes.size();
		return (nConnections < nActivations*nTotal); //the graph is not maxed out
	}

	@Override
	public Individual apply(Individual before) {
		//first, how many new connections are possible?
		int nConnections = before.getReactionNetwork().connections.size();
		int nActivations = before.getReactionNetwork().getNSimpleSequences();
		int nTotal = before.getReactionNetwork().nodes.size();
		int possibilities = nActivations*nTotal - nConnections;
		
		//pick one
		int newConnectionIndex = (int) Tools.getRandomLinearScale(0, possibilities);
		
		//now iterate through all the nodes in order, until we reach that connection
		int counter = 0;
		Node from = null;
		Node to = null;
		do{
			for(int fr=0; fr<nActivations; fr++){
				if(before.getReactionNetwork().nodes.get(fr).type!=Node.SIMPLE_SEQUENCE){
					continue; //not an activator, so skip
				}
				from = before.getReactionNetwork().nodes.get(fr);
				for(int t=0; t<nTotal; t++){
					to = before.getReactionNetwork().nodes.get(t);
					if(before.getReactionNetwork().getConnectionByEnds(from, to)==null){
						counter++; //we found a potential new connection
					}
				}
			}
		} while(counter<newConnectionIndex);
		
		if(from!=null && to!=null){
			before.addConnection(from.name, to.name);
		} else {
			System.err.println("AddTemplate: failed to find a valid new connection");
		}
		
		return before;
	}

}
