package evaluator.descriptor.phenotype;

import java.util.ArrayList;

import individuals.Individual;

/**
 * Sums the apparent activity in all places.
 * May be normalized to compare systems with different dimensions
 * @author naubertkato
 *
 */
public class TotalActivityDescriptor extends PhenotypeDescriptor{
	
	boolean normalize;

	protected TotalActivityDescriptor(Individual indiv, int dim, ArrayList<Object> dat, boolean normalize) {
		super(indiv, dim, dat);
		this.normalize = normalize;
		System.err.println("WARNING: dummy evaluator");
	}

	protected TotalActivityDescriptor(Individual indiv, int dim, ArrayList<Object> dat) {
		this(indiv,dim,dat,false);
	}
	
	
	
}
