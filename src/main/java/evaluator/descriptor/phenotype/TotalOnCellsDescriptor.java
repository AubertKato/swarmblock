package evaluator.descriptor.phenotype;

import java.util.ArrayList;

import individuals.Individual;

public class TotalOnCellsDescriptor extends PhenotypeDescriptor {
	
	protected int bufferedResult = 0;

	protected TotalOnCellsDescriptor(Individual indiv, boolean[][] cells) {
		super(indiv, 2, cellsToDat(cells));
		
		//Innefficient to go through it twice, but no other way.
		for(int i = 0; i<cells.length; i++) {
			for(int j = 0; j<cells[i].length; j++) {
				if(cells[i][j]) {
					bufferedResult++;
				}
				
			}
		}
	}

	
	@Override
	public double getValue() {
		return bufferedResult;
	}
	
	protected static ArrayList<Object> cellsToDat(boolean[][] cells){
		ArrayList<Object> ret = new ArrayList<Object>();
		for(int i = 0; i<cells.length; i++) {
			ArrayList<Double> arr = new ArrayList<Double>();
			for(int j = 0; j<cells[i].length; j++) {
				if(cells[i][j]) {
					arr.add(1.0);
				} else {
					arr.add(0.0);
				}
				
			}
			ret.add(arr);
		}
			
			return ret;
	}
}
