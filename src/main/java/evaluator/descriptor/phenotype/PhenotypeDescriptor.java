package evaluator.descriptor.phenotype;

import java.util.ArrayList;

import evaluator.descriptor.AbstractDescriptor;
import individuals.Individual;

public class PhenotypeDescriptor extends AbstractDescriptor {

    protected Individual individual;
    protected int dimensions;
    //The evaluation result. Done somewhere else;
    protected ArrayList<Object> data;
	
	protected PhenotypeDescriptor(Individual indiv, int dim, ArrayList<Object> dat) {
		individual = indiv;
		dimensions = dim;
		data = dat;
		init();
	}
	
	protected void init() {
		name = this.getClass().getSimpleName();
	}
	
	public int getDimensions() {
		return dimensions;
	}
	
	public ArrayList<Object> getData(){
		return data;
	}
	
}
