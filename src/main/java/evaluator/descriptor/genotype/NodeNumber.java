package evaluator.descriptor.genotype;

import individuals.Individual;

public class NodeNumber extends GenotypeDescriptor {

	public NodeNumber(Individual indiv) {
		super(indiv);
	}
	
	@Override
	public double getValue() {
		if(individual == null) return 0;
		int numberOfNodes = individual.getReactionNetwork().nodes.size();
		return (double) numberOfNodes;
	}

}
