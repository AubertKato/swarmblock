package evaluator.descriptor.genotype;

import evaluator.descriptor.AbstractDescriptor;
import individuals.Individual;

/**
 * Genotype descriptors are based solely on analyzing the reaction network of an individual.
 * No simulation (should be) required.
 * Similar to the idea of static code analyzis. 
 * @author naubertkato
 *
 */
public class GenotypeDescriptor extends AbstractDescriptor {

	protected Individual individual;
	
	protected GenotypeDescriptor(Individual indiv) {
		individual = indiv;
		init();
	}
	
	protected void init() {
		name = this.getClass().getSimpleName();
	}
	

}
