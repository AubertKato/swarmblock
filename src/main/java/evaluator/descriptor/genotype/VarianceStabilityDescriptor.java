package evaluator.descriptor.genotype;

import individuals.Individual;
import individuals.reactionnetwork.Node;

public class VarianceStabilityDescriptor extends GenotypeDescriptor {

	protected VarianceStabilityDescriptor(Individual indiv) {
		super(indiv);
	}

	@Override
	public double getValue() {
		if(individual == null) return 0;
		double variance = 0.0;
		double average = 0.0;
		double nNodes = 0.0;
		for(Node n : individual.getReactionNetwork().nodes) {
			if(n.type == Node.SIMPLE_SEQUENCE) {
				average += n.parameter;
				variance += n.parameter*n.parameter;
				nNodes += 1.0;
			}
		}
		
		if (nNodes <= 0.0) return 0.0;
		
		average /= nNodes;
		variance = variance/nNodes - average*average; //means of square minus square of means
		
		return Math.sqrt(variance);
	}
	
}
