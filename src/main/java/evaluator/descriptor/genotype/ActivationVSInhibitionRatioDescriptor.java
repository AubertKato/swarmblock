package evaluator.descriptor.genotype;

import individuals.Individual;
import individuals.reactionnetwork.Connection;
import individuals.reactionnetwork.Node;

public class ActivationVSInhibitionRatioDescriptor extends GenotypeDescriptor {

	protected ActivationVSInhibitionRatioDescriptor(Individual indiv) {
		super(indiv);
	}
	
	@Override
	public double getValue() {
		if(individual == null) return 0;
		double activations = 0;
		double inhibitions = 0;
		for(Connection c : individual.getReactionNetwork().connections) {
			if(c.enabled) {
				if(c.to.type == Node.INHIBITING_SEQUENCE) {
					inhibitions += 1;
				} else {
					activations += 1;
				}
			}
		}
		
		return (inhibitions==0?1.0:activations/inhibitions);
	}
	
}
