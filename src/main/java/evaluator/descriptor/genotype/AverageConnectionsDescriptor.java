package evaluator.descriptor.genotype;

import individuals.Individual;
import individuals.reactionnetwork.Connection;

public class AverageConnectionsDescriptor extends GenotypeDescriptor {

	protected AverageConnectionsDescriptor(Individual indiv) {
		super(indiv);
		
	}
	
	@Override
	public double getValue() {
		if(individual == null) return 0;
		double nodeNumber = individual.getReactionNetwork().nodes.size();
		double connectionNumber = 0.0;
		for(Connection c : individual.getReactionNetwork().connections) {
			if(c.enabled) {
				connectionNumber += 1;
				if(!c.to.equals(c.from)) {
					connectionNumber += 1; //counting both input and output, EXCEPT if autocatalysis.
				}
			}
		}
		return (nodeNumber==0.0?0.0:connectionNumber/nodeNumber);
		
	}
	
	public static void main(String[] args) {
		AverageConnectionsDescriptor g = new AverageConnectionsDescriptor(null);
		System.out.println(g);
		
		Individual a = Individual.getTestIndividual();
		g = new AverageConnectionsDescriptor(a);
		System.out.println(g);
	}
	
}
