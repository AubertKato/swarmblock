package evaluator.descriptor.genotype;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import generator.Generator;
import individuals.Individual;
import individuals.reactionnetwork.Connection;
import individuals.reactionnetwork.Node;
import individuals.reactionnetwork.ReactionNetwork;

public class DiameterDescriptor extends GenotypeDescriptor {

	protected DiameterDescriptor(Individual indiv) {
		super(indiv);
	}

	@Override
	public double getValue() {
		
		return computeDiameter();
	}
	
	
	/**
	 * Implementation of the algorithm.
	 * Uses a dynamic approach, where each node tries to update the longest path of nodes downstream.
	 * We keep going until the system has converged.
	 * We consider inhibition to be a connection to the output of inhibited template, since it's the one being affected.
	 * @return
	 */
	private int computeDiameter() {
		ReactionNetwork rn = individual.getReactionNetwork();
		
		//First, initialization
		HashMap<Node, Path> paths = new HashMap<Node,Path>();
		HashMap<Node, Path> lastSteppaths = new HashMap<Node,Path>(); //not the current one, but the latest update.
		//Construct an easy data structure to get the nodes outputs.
		HashMap<Node, Set<Node>> connectedTo = new HashMap<Node, Set<Node>>(); //uses sets, so that repeats aren't a problem.
		for(Node n: rn.nodes){
			paths.put(n, new Path(n));
			HashSet<Node> downstream = new HashSet<Node>();
			
			//add the connection of inhibitors. For activator, it's more straightforward to iterate over connections
			if(n.type == Node.INHIBITING_SEQUENCE) {
				Connection c = individual.getReactionNetwork().getConnectionByInhibitor(n);
				if(c.enabled) {
					downstream.add(c.to);
				}
			}
			
			connectedTo.put(n, downstream);
		}
		
		//Going through connections to add downstream stuff.
		for(Connection c : rn.connections) {
			if(c.enabled) {
				connectedTo.get(c.from).add(c.to);
			}
		}
		
		// update as much as we can.
		HashSet<Node> toUpdate = new HashSet<Node>();
		toUpdate.addAll(rn.nodes);
		while(toUpdate.size() != 0) {
			HashSet<Node> toUpdateNext = new HashSet<Node>();
			
			for(Node n: rn.nodes){
				Path pathn;
				if(lastSteppaths.containsKey(n)) {
					pathn = lastSteppaths.get(n);
				} else {
				    pathn = paths.get(n);
				}
				
				for(Node m: connectedTo.get(n)) {
					Path updpathm = paths.get(m);
					Path currentVer = updpathm.clone();
					Path next = updpathm.getPathUpdate(pathn);
					if(next!=null && !next.equals(lastSteppaths.get(m))  && !next.equals(currentVer)) {
						//debug
						//System.out.println(updpathm.toString()+" "+next.toString());
						lastSteppaths.put(m,next); //may be different, but relevant to the descendants.
						toUpdateNext.add(m);
					}
				}
			}
			toUpdate = toUpdateNext;
		}
		
		int size = -1;
		for(Node n: rn.nodes) {
			size = Math.max(size, paths.get(n).size());
			System.out.println(paths.get(n));
		}
		
		return size;
	}
	
	private class Path implements Cloneable {
		Node myNode;
		ArrayList<Node> currentPath = new ArrayList<Node>(); // goes backward, for ease of manipulation.
		
		
		Path(Node start){
			myNode = start;
		    currentPath.add(start);	
		}
		
		/**
		 * Upgrade the current path and return it. Returns null if the path is unchanged.
		 * @param incoming
		 * @return
		 */
		Path getPathUpdate(Path incoming){
			Path p = null;
			Path t = append(myNode,incoming);
			if(incoming.currentPath.size()>=currentPath.size() && !incoming.currentPath.contains(myNode)) { 
				// we should have +1 compared to them, but we don't
				//also, that's not a loop
				currentPath = new ArrayList<Node>();

				//deep copy
				for(int i = 0; i<t.currentPath.size();i++) {
					currentPath.add(t.currentPath.get(i)); //would be more elegant to addAll, but I want to be sure of the order.
				}
				p = this;
			} else if(incoming.currentPath.size()==currentPath.size()-1 && !incoming.currentPath.contains(myNode)) {
				// I don't need to update, but people beyond me may find it to be a longer path if the current one involves them.
			   p = t;
			}
			return p;
		}
		
		public Path clone() {
			Path p = new Path(myNode);
			for(int i=1;i<currentPath.size();i++) {
				p.currentPath.add(currentPath.get(i));
			}
			return p;
		}
		
		private Path append(Node n, Path path) {
			Path p = new Path(n);
			for(int i=0;i<path.currentPath.size();i++) {
				p.currentPath.add(path.currentPath.get(i));
			}
			return p;
		}
		
		public int size() {
			return currentPath.size();
		}
		
		public String toString() {
			StringBuilder build = new StringBuilder("Path[");
			for(int i = currentPath.size()-1; i>0; i--) {
				build.append(currentPath.get(i).name+",");
			}
			if(currentPath.size()>0) {
				build.append(currentPath.get(0).name);
			}
			build.append("]");
			
			return build.toString();
		}
		
		@Override
		public boolean equals(Object o) {
			if(o!=null && Path.class.isAssignableFrom(o.getClass())) {
				Path po = (Path) o;
				boolean test = (this.myNode == po.myNode)&&this.currentPath.size() == po.currentPath.size();
				if(test) {
					for(int i = 0; i<currentPath.size(); i++) {
						if(currentPath.get(i)!=po.currentPath.get(i)) {
							return false;
						}
					}
					return true;
				}
			}
			return false;
		}
	}
	
	public static void main(String[] args) {
		DiameterDescriptor test = new DiameterDescriptor(Individual.getTestIndividual());
		
		System.out.println("Super basic test: "+test.getValue());
		
		Generator gen = new Generator();
		
		for(int i = 15; i <= 15; i++) {
			for(int j = i; j<= i; j++) {
				for(int k = 0; k<10; k++) {
					Individual indiv = gen.generate(i, j);
					if(indiv != null) {
					System.out.println("==============");
					
				    System.out.println(indiv.getReactionNetwork());
					test = new DiameterDescriptor(indiv);
					
					System.out.println("Super basic test: "+test.getValue());
					}
					
				}
			}
		}
		
		
	}
	
}
