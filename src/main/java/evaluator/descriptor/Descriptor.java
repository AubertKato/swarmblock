package evaluator.descriptor;

public interface Descriptor {

	public String getName();
	
	public double getValue();
	
}
