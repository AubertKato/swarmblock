package evaluator.descriptor;


/**
 * This class is a dummy descriptor for tests.
 * Returned value is always 0
 * @author naubertkato
 *
 */
public class ZeroDescriptor extends AbstractDescriptor {

	public ZeroDescriptor() {
		name = "zero";
	}
	
	@Override
	public double getValue() {
		
		return 0;
	}
}
