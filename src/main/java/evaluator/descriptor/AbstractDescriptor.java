package evaluator.descriptor;

public abstract class AbstractDescriptor implements Descriptor {

	protected String name;
	
	@Override
	public String getName() {
		return name;
	}

	@Override
	public double getValue() {
		
		return -1;
	}
	
	public String toString() {
		return "[ name: "+getName()+", value: "+getValue()+"]";
	}

}
