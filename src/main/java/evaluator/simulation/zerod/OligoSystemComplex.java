package evaluator.simulation.zerod;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import evaluator.simulation.Simulation;
import model.Constants;
import model.ECFRNTemplateFactory;
import model.OligoSystemWithProtectedSequences;
import model.PseudoTemplateGraph;
import model.PseudoTemplateOligoSystem;
import model.SaturationEvaluator;
import model.SaturationEvaluatorProtected;
import model.chemicals.ProtectedSequenceVertex;
import model.chemicals.PseudoExtendedSequence;
import model.chemicals.ReporterIndicator;
import model.chemicals.SequenceVertex;
import individuals.reactionnetwork.Connection;
import individuals.reactionnetwork.Node;
import individuals.reactionnetwork.ReactionNetwork;
import individuals.reactionnetwork.ReactionNetworkContainer;
import utils.EdgeFactory;
import utils.PseudoTemplateCapableTemplateFactory;
import utils.VertexFactory;

public class OligoSystemComplex implements ReactionNetworkContainer, Simulation {
	
	public static boolean selfStart = false; //transmitted to the embedded graph.
	
	/**
	 * Buffer for multi evaluations
	 */
	Map<String, double[]> result = new HashMap<String, double[]>();
	protected boolean bufferValid = false;
	
	protected PseudoTemplateGraph<SequenceVertex,String> graph;
	protected ReactionNetwork network; 
	protected HashMap<String,SequenceVertex> equiv = new HashMap<String,SequenceVertex>();
	protected double[] polKm = {0.0, Constants.polKm, 0.0, Constants.polKmBoth, 0.0,0.0,0.0,0.0};
    protected double[] nickKm = {0.0, 0.0, 0.0, 0.0, Constants.nickKm,0.0,0.0,0.0};
    protected double[] exoKm = {0.0, 0.0, 0.0, 0.0, 0.0,0.0,Constants.exoKmSimple,Constants.exoKmInhib};
	
	public OligoSystemComplex(ReactionNetwork network) {
		
		this.network = network;
		
		//First step: create an empty oligograph:
		initGraph();
		
		//Second step: populate with all sequences ("nodes") from the network
		for(Node n : network.nodes){
			if(n.type == Node.INHIBITING_SEQUENCE){
				Connection inhibited = network.getConnectionByInhibitor(n);
				if (inhibited==null || !inhibited.enabled) continue; // we ignore the species
				if (utils.Constants.use6_7rule) n.parameter = Node.get6_7ruleStability(inhibited.from, inhibited.to);// just in case target stability changed
			}
			SequenceVertex s = graph.getVertexFactory().create();
			s.initialConcentration = n.initialConcentration;
			if(n.protectedSequence){
				s = new ProtectedSequenceVertex(s.ID,s.initialConcentration);
			}
			if(n.reporter){
				graph.addActivation("r"+s.ID,s,ReporterIndicator.indicator,ReporterIndicator.reporterConcentration);
			}
			equiv.put(n.name, s);
			s.setInhib(n.type == Node.INHIBITING_SEQUENCE);
			
			graph.addSpecies(s,n.parameter,n.initialConcentration);
			
			//For pseudoTemplates
			if(n.hasPseudoTemplate){
				PseudoExtendedSequence newi = new PseudoExtendedSequence(s,0.0);
				graph.setExtendedSpecies(s, newi);
				graph.setTemplateConcentration(graph.getEdgeFactory().createEdge(s, newi), n.pseudoTemplateConcentration);
			}
			
		}
		
		//Third step: add connections
		for(Connection c : network.connections){
			if (c.enabled) {
				String name = c.from.name+c.to.name;
				if(equiv.get(c.from.name)== null || equiv.get(c.to.name)== null){
					//Somehow one of the ends was deleted, and this connection was reactivated afterwards.
					continue;
				}
				
				graph.addActivation(name, equiv.get(c.from.name), equiv.get(c.to.name), c.parameter);
				if(!c.from.DNAString.equals("")&!c.to.DNAString.equals("")){
					String atTheNick = ""+c.from.DNAString.charAt(c.from.DNAString.length()-1);
					atTheNick += c.to.DNAString.charAt(0);
					//System.out.println(atTheNick);
					double[] slow = model.SlowdownConstants.getSlowdown(atTheNick);
					graph.setStacking(name,slow[0]);
					graph.setDangleL(name,slow[1]);
					graph.setDangleR(name,slow[2]);
				}	
			}
		}
		
		//Fourth step: add inhibitions
		for(Node n : network.nodes){
			if(n.type == Node.INHIBITING_SEQUENCE){
				Connection c = network.getConnectionByInhibitor(n);
				if (c == null || !c.enabled){
					System.err.println("OligoSystemComplex: init graph ERROR: invalid inhibitor "+n.name);
				}
				else{
					String inhibited = c.from.name+c.to.name;
				    SequenceVertex v = equiv.get(n.name);
				    if (v != null) graph.addInhibition(inhibited, v);
				}
			}
		}
		
		//Fifth step: other parameters? TODO
		//Specifically, we should change the kms above...
		graph.saturableExo = true;
		graph.saturableNick = true;
		graph.saturablePoly = true;
		graph.dangle = true;
		//if(this.graph.exoSaturationByFreeTemplates){
	    //	exoKm[SaturationEvaluator.TALONE] = Constants.exoKmTemplate;
	    //}
		
		//TODO: other params that could (should?) be evolved: missing bases.
		
/*		for(String param: network.parameters.keySet()){
			//only a few params are known:
			if(param.equals("nick")){
				Constants.nickVm = network.parameters.get(param)*Constants.nickKm;
			} else if(param.equals("pol")){
				Constants.polVm = network.parameters.get(param)*Constants.polKm;
			} else if(param.equals("exo")){
				Constants.exoVm = network.parameters.get(param)*Constants.exoKmSimple;
			}
		}*/ //Not sure we are evolving the enzyme conc...

	}
	
	public void toggleExoSaturationByAll(){
		if(exoKm[1]==0.0){
			for(int i=1; i<SaturationEvaluator.SIGNAL; i++ ){
				exoKm[i] = exoKm[0];
			}
		} else {
			for(int i=1; i<SaturationEvaluator.SIGNAL; i++ ){
				exoKm[i] = 0.0;
			}
		}
	}
	
	protected void initGraph(){
		final PseudoTemplateGraph<SequenceVertex, String> g = new PseudoTemplateGraph<SequenceVertex,String>();
		
		g.setSaturations(new SaturationEvaluatorProtected<String>(polKm,nickKm,exoKm));
	    g.initFactories(new VertexFactory<SequenceVertex>(g){
	    	
			public SequenceVertex create() {
				SequenceVertex newvertex = associatedGraph.popAvailableVertex();
				if (newvertex == null){
					newvertex = new SequenceVertex(associatedGraph.getVertexCount() + 1);
				} else {
					newvertex = new SequenceVertex(newvertex.ID);
				}
				return newvertex;
			}

			@Override
			public SequenceVertex copy(SequenceVertex original) {
				 SequenceVertex ret = new SequenceVertex(original.ID);
				 ret.inputs = original.inputs;
				 return ret;
			} 	
	    },new EdgeFactory<SequenceVertex,String>(g){
	    	public String createEdge(SequenceVertex v1, SequenceVertex v2){
	    		if(PseudoExtendedSequence.class.isAssignableFrom(v2.getClass())){
	    			return "Pseudo"+v1.ID;
	    		}
	    		return v1.ID+"->"+v2.ID;
	    	}
	    	public String inhibitorName(String s){
	    		return "Inhib"+s;
	    	}
	    });
	    g.selfStart = selfStart;
	    this.graph = g;
	}
	
	protected int getTrueIndex(ArrayList<SequenceVertex> list,SequenceVertex s){
		 int where = 0;
		if (s == null) {
            return -1;
        } else {
            for (int i = 0; i < list.size(); i++){
                if (s.equals(list.get(i))){
                    return where;
                }
            where++;
            if(ProtectedSequenceVertex.class.isAssignableFrom(list.get(i).getClass())) where++;
            }
           
        }
		return -1;
	}
	
	protected double[] arraySum(double[] a1, double[] a2){
		double[] a = new double[Math.min(a1.length, a2.length)];
		for (int i=0; i<a.length;i++){
			a[i] = a1[i] + a2[i];
		}
		return a;
	}

	public Map<String, double[]> calculateTimeSeries() {
		return calculateTimeSeries(-1); //for legacy/tests.
	}
	
	public Map<String, double[]> calculateTimeSeries(int timeOut) {
		if(!bufferValid){
		  OligoSystemWithProtectedSequences<String> myOligo = new OligoSystemWithProtectedSequences<String>(new PseudoTemplateOligoSystem(graph,new ECFRNTemplateFactory<String>(graph, new PseudoTemplateCapableTemplateFactory<String>(graph)),graph.se));
		  myOligo.timeOut = timeOut;
		  double[][] timeTrace = myOligo.calculateTimeSeries();
		  for(Node n : this.network.nodes){
			SequenceVertex s = equiv.get(n.name);
			int index = getTrueIndex(myOligo.getSequences(),s);
			if (index == -1) continue; // This DNA strand does not exist in the system
			if(ProtectedSequenceVertex.class.isAssignableFrom(s.getClass())){
				result.put(n.name, arraySum(timeTrace[index],timeTrace[index+1]));
			} else {
				result.put(n.name, timeTrace[index]);
			}
			if(n.reporter){
				result.put("Reporter "+n.name,timeTrace[myOligo.total+myOligo.inhTotal+myOligo.getReporterIndex(s)]);
			}
		  }
		}
		return result;
	}
	
	public PseudoTemplateGraph<SequenceVertex,String> getGraph(){
		return graph;
	}
	
	public HashMap<String, SequenceVertex> getEquiv(){
		return equiv;
	}
	
	public ReactionNetwork getReactionNetwork(){
		return network;
	}

	@Override
	public int getDimensions() {
		//We are 0D
		return 0;
	}

	@Override
	public double[][] getData(int parameter) {
		Map<String,double[]> res = calculateTimeSeries(); //might return the buffer
		double[][] data = new double[res.size()][];
		Iterator<Node> it = network.nodes.iterator();
		int where = 0;
		while(it.hasNext()){
			String n = it.next().name;
			if(res.containsKey(n)){
			  data[where] = res.get(n);
			  where++;
			}
		}
		return data;
	}
	
	@Override
	public void invalidate(){
	  bufferValid = false;
	}
}
