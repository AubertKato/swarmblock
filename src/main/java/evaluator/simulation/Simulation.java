package evaluator.simulation;

public interface Simulation {
	
	/**
	 * 
	 * @return the size of physical system, 0 to 3
	 */
	int getDimensions();
	
	/**
	 * Depending on the physical size of the system, parameter may indicate different things (or be a dummy)
	 * @param parameter 
	 * @return
	 */
	double[][] getData(int parameter);
	
	/**
	 * Dispose of the current buffer. Should be called if change is made to the underlying reaction network
	 */
	void invalidate();

}
