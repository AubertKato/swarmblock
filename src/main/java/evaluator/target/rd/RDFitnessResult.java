package evaluator.target.rd;

import evaluator.target.AbstractFitnessResult;

public abstract class RDFitnessResult extends AbstractFitnessResult {
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public abstract float[][][] getConc();
	
	public abstract boolean[][] getPattern();
	
	public abstract boolean[][] getPositions();

}
