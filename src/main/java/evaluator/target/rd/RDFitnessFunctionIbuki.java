package evaluator.target.rd;

import java.util.Arrays;

import evaluator.simulation.rd.RDConstants;
import evaluator.simulation.rd.RDSystem;
import evaluator.target.AbstractFitnessResult;
import individuals.reactionnetwork.ReactionNetwork;

public class RDFitnessFunctionIbuki extends RDFitnessFunction{
 private static final long serialVersionUID=-261536139595844890L;
 
 
 public RDFitnessFunctionIbuki(boolean[][] pattern){
  super(pattern);
 }
 /**
  * Basically the same of the original class.
  * It returns my original fitness result object. 
  */
 @Override public AbstractFitnessResult evaluate(ReactionNetwork network){
  long startTime=System.currentTimeMillis();
  AbstractFitnessResult[] results = new AbstractFitnessResult[RDConstants.reEvaluation];
  //System.out.println(network);
  for(int i= 0; i<RDConstants.reEvaluation; i++){
  RDSystem syst=new RDSystem();
  syst.buildFormNetwork(network);
  syst.init(false); // no GUI
  syst.evaluate(utils.Constants.maxEvalTime);
  //System.out.println(Arrays.toString(syst.conc[3][10]));
  if(RDConstants.timing){
   System.out.println("total time: "+(System.currentTimeMillis()-startTime));
   System.out.println("total bead update:"+syst.totalBeads);
   System.out.println("total conc update:"+syst.totalConc);
  }
  RDPatternFitnessResultIbuki temp = new RDPatternFitnessResultIbuki(syst.conc,pattern,syst.beadsOnSpot,randomFitness);
  results[i] = temp;
  }
  
  Arrays.sort(results, new AbstractFitnessResult.AbstractFitnessResultComparator());
  if (RDConstants.useMedian) return results[(RDConstants.reEvaluation-1)/2];
  return results[0];
 }
 
}
 
