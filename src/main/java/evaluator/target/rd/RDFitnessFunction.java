package evaluator.target.rd;


import java.util.Arrays;

import evaluator.simulation.rd.RDConstants;
import evaluator.simulation.rd.RDSystem;
import evaluator.target.AbstractFitnessFunction;
import evaluator.target.AbstractFitnessResult;
import individuals.reactionnetwork.ReactionNetwork;
import model.OligoGraph;
import model.OligoSystem;
import model.chemicals.SequenceVertex;
import utils.PadiracTemplateFactory;

public class RDFitnessFunction extends AbstractFitnessFunction {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8422873429506377841L;
	
	protected boolean[][] pattern;
	protected double randomFitness;
	protected static final RDPatternFitnessResult minFitness = RDPatternFitnessResult.getMinFitness();
	
	protected RDFitnessFunction(){
		
	}
	
	public RDFitnessFunction(boolean[][] pattern){
		this.pattern = pattern;
		randomFitness = RDConstants.defaultRandomFitness;
		if (randomFitness < 0) randomFitness = 0.0;
	}
	
	protected AbstractFitnessResult computeResult(RDSystem syst){
		return new RDPatternFitnessResult(syst.conc,pattern,syst.beadsOnSpot, randomFitness);
	}

	@Override
	public AbstractFitnessResult evaluate(ReactionNetwork network) {
		AbstractFitnessResult[] results = multipleEvaluation(network);
		  
		  Arrays.sort(results, new AbstractFitnessResult.AbstractFitnessResultComparator());
		  if (RDConstants.useMedian) return results[(RDConstants.reEvaluation-1)/2];
		  return results[0];
	}
	
	protected AbstractFitnessResult[] multipleEvaluation(ReactionNetwork network){
		long startTime = System.currentTimeMillis();
		AbstractFitnessResult[] results = new AbstractFitnessResult[RDConstants.reEvaluation];
		  for(int i= 0; i<RDConstants.reEvaluation; i++){
		RDSystem syst = new RDSystem();
		
		syst.buildFormNetwork(network);
		  syst.init(false); //no GUI
		  
		  syst.evaluate(utils.Constants.maxEvalTime);
		  if(RDConstants.timing){
			  System.out.println("total time: "+(System.currentTimeMillis()-startTime));
			  System.out.println("total bead update:"+syst.totalBeads);
			  System.out.println("total conc update:"+syst.totalConc);
		  }
		  AbstractFitnessResult temp =  computeResult(syst);
		  results[i] = temp;
		  }
		  return results;
	}
	
	public AbstractFitnessResult evaluate(OligoGraph<SequenceVertex,String> g){
		long startTime = System.currentTimeMillis();
		RDSystem syst = new RDSystem();
		syst.setNetwork(null); //TODO: should have a reverse engineering function
		syst.setOS(new OligoSystem<String>(g, new PadiracTemplateFactory(g)));
		  syst.init(false); //no GUI
		  
		  syst.evaluate(utils.Constants.maxEvalTime);
		  if(RDConstants.timing){
			  System.out.println("total time: "+(System.currentTimeMillis()-startTime));
			  System.out.println("total bead update:"+syst.totalBeads);
			  System.out.println("total conc update:"+syst.totalConc);
		  }
		return new RDPatternFitnessResult(syst.conc,pattern,syst.beadsOnSpot, randomFitness);
	}

	@Override
	public AbstractFitnessResult minFitness() {
		
		return minFitness;
	}

}
