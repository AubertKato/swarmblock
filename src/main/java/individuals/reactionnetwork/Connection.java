package individuals.reactionnetwork;

import java.io.Serializable;

public class Connection implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public int innovation;
	public Node from;
	public Node to;
	public Node inhib = null;
	public boolean enabled = true;
	public double parameter = 0;

	public Connection(Node from, Node to) {
		this.from = from;
		this.to = to;
	}
	
	public Connection(Node from, Node to, Node inhib) {
		this.from = from;
		this.to = to;
		this.inhib = inhib;
	}
	
	@Override
	public boolean equals(Object o){
		if(o.getClass()!= this.getClass()){
			return false;
		}
		Connection c = (Connection) o;
		return c.innovation == innovation && Node.equals(from,c.from) && Node.equals(to,c.to) && Node.equals(inhib,c.inhib) && c.enabled == enabled
				&& c.parameter == parameter;
	}
	
	
	
	@Override
	public int hashCode(){
		int result = 349;
		int prime = 37;
		result = prime*result + innovation;
		result = prime*result + (from == null? 0 : from.hashCode());
		result = prime*result + (to == null ? 0 :to.hashCode());
		result = prime*result + (inhib == null ? 0 : inhib.hashCode());
		result = prime*result + (enabled?1:0);
		long f = Double.doubleToLongBits(parameter);
		result = prime * result + (int) (f ^ (f>>>32));
		return result;
	}
	
	public static void main(String[] args){
		Connection a = new Connection(null,null,null);
		System.out.println(a.hashCode());
		System.out.println(a.equals(a));
	}

}
