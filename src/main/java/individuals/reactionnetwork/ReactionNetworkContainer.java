package individuals.reactionnetwork;

public interface ReactionNetworkContainer {

	public ReactionNetwork getReactionNetwork();
	
}
