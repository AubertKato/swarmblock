package individuals;

import java.util.concurrent.atomic.AtomicInteger;

import individuals.reactionnetwork.Connection;
import individuals.reactionnetwork.Node;
import individuals.reactionnetwork.ReactionNetwork;
import individuals.reactionnetwork.ReactionNetworkContainer;
import utils.Constants;
import utils.Tools;

/**
 * Wrapper class for reactionnetworks
 * @author naubertkato
 *
 */
public class Individual implements ReactionNetworkContainer {
	
	
	private int ID;
	protected ReactionNetwork network;
	protected static AtomicInteger innovation = new AtomicInteger(0);
	
	public Individual (int ID, ReactionNetwork network){
		this.ID = ID;
		this.network = network;
	}
	
	public Individual (int ID){
		this.ID = ID;
		this.network = new ReactionNetwork();
	}
	
	@Override
	public ReactionNetwork getReactionNetwork() {
		return network;
	}
	
	public void setReactionNetwork(ReactionNetwork network){
		this.network = network;
	}
	
	/**
	 * Adds an activator with name {@code name} if available.
	 * Otherwise, does nothing.
	 * @param name
	 */
	public void addActivator(String name){
		if(network.getNodeByName(name)==null){
			Node node = new Node(name);
			node.parameter = Tools.getRandomLogScale(Constants.minReactionNetworkStabilityValue, Constants.maxReactionNetworkStabilityValue);
			network.addNode(node);
		}
	}
	
	/**
	 * Creates a new valid activator
	 * @return the new activator's name
	 */
	public String addActivator(){
		int nSize = network.nodes.size();
		int preName = nSize;
		String name = ""+preName;
		while(network.getNodeByName(name)!=null){
			preName++;
			name = ""+preName;
		}
		addActivator(name);
		return name;
	}
	
	/**
	 * Does all the dirty work to add a newly created inhibitor species for the template going from {@code from} to {@code to}
	 * @param from
	 * @param to
	 */
	public void addInhibitor(String from, String to){
		Node nfrom = network.getNodeByName(from);
		Node nto = network.getNodeByName(to);
		if(nfrom != null && nto != null){
			Connection conn = network.getConnectionByEnds(nfrom, nto);
			if(conn != null && conn.inhib == null){
				Node node = new Node(Node.getInhibitorName(from, to),Node.INHIBITING_SEQUENCE);
				node.parameter = (utils.Constants.use6_7rule?Node.get6_7ruleStability(nfrom, nto):
					Tools.getRandomLogScale(Constants.minReactionNetworkStabilityValue, Constants.maxReactionNetworkStabilityValue));
				conn.inhib = network.addNode(node);
			}
		}
	}
	
	/**
	 * Does all the dirty work to add a template going from {@code from} to {@code to}
	 * @param from
	 * @param to
	 */
	public void addConnection(String from, String to){
		Node nfrom = network.getNodeByName(from);
		Node nto = network.getNodeByName(to);
		if(nfrom != null && nto != null && network.getConnectionByEnds(nfrom, nto)==null){
			Connection conn = network.addConnection(innovation.getAndIncrement(), nfrom, nto);
			if(conn != null){
				conn.parameter = Tools.getRandomLinearScale(Constants.minReactionNetworkTemplateValue, Constants.maxReactionNetworkTemplateValue);
			} else {
				System.err.println("Connection creation failed between "+nfrom.toString()+" and "+nto.toString());
			}
		}
	}
	
	/**
	 * Removes the template from {@code from} to {@code to}.
	 * Note that this leaves in place any inhibitor related to that template.
	 * @param from
	 * @param to
	 */
	public void removeConnection(String from, String to){
		Node nfrom = network.getNodeByName(from);
		Node nto = network.getNodeByName(to);
		if(nfrom != null && nto != null ){
			Connection conn = network.getConnectionByEnds(nfrom, nto);
			if(conn != null){
				network.connections.remove(conn);
			} else {
				System.err.println("Connection removal failed between "+nfrom.toString()+" and "+nto.toString());
			}
		}
	}
	
	public int getID(){
		return ID;
	}
	
	public String toString() {
		StringBuilder acc = new StringBuilder(""+getID()+"\n");
		acc.append(network.toString());
		return acc.toString();
	}
	
	/**
	 * Generates an Individual with ID 0 and the Oligator as reaction network.
	 * Uses default Template concentrations and Species stability
	 * @return the generated Individual
	 */
	public static Individual getTestIndividual() {
		Individual test = new Individual(0);
		String from = test.addActivator();
		String to = test.addActivator();
		test.addConnection(from, from);
		test.addConnection(from, to);
	    test.addInhibitor(from, from);
		String inhib = Node.getInhibitorName(from, from);
		test.addConnection(to, inhib);
		return test;
	}

}
